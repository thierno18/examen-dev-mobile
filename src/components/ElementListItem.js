import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';

import Colors from "../definitions/Colors";

const ElementListItem = ({onClick, element, isFav = false}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => {onClick(element.id)}}>
            <Image source={{uri: "https://image.tmdb.org/t/p/w500/" + element.poster_path}}   style={styles.thumbnail}/>
            <View style={styles.informationContainer}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>
                        {element.original_title}
                    </Text>
                    <Text style={[styles.data, styles.text]}
                          numberOfLines={1}>
                        {element.release_date}
                    </Text>
                </View>
                <View style={styles.statContainer}>
                    <Text style={[styles.data]}>
                        {element.overview}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>

    );
};


export default ElementListItem;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingVertical: 8,
    },
    informationContainer: {
        flex: 1,
        marginLeft: 12,
        justifyContent: 'center',
    },
    noThumbnailContainer: {
        width: 128,
        height: 128,
        alignItems: 'center',
        justifyContent: 'center',
    },
    thumbnail: {
        width: 128,
        height: 128,
        borderRadius: 12,
        backgroundColor: Colors.mainGreen,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    data: {
        fontSize: 16,
    },
    text: {
        fontStyle: 'italic',
    },
});

