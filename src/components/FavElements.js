import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';


import ElementListItem from "../components/ElementListItem"
import DisplayError from '../components/DisplayError';


import {getFilmDetails} from "../api/myApi";

const FavElements = ({ navigation, favElements }) => {

    const [elements, setElements] = useState([]);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        refreshFavFilms();
    }, [favElements]);

    const refreshFavFilms = async () => {
        setIsRefreshing(true);
        setIsError(false);
        let elements = [];
        try {
            for (const id of favElements) {
                const apiSearchResult = await getFilmDetails(id)
                elements.push(apiSearchResult);
            };
            setElements(elements);
        } catch (error) {
            setIsError(true);
            setElements([]);
        }
        setIsRefreshing(false);
    };

    const navigateToFilmeDetails = (elementID) => {

        navigation.navigate("ViewFilme", {elementID});
    };

    const amIaFavFilme = (elementID) => {
            if (favElements.findIndex(i => i === elementID) !== -1) {
                return true;
            }


        return false;
    };

    return (
        <View style={styles.container}>
            {
                isError ?
                    (<DisplayError message='Impossible de récupérer les elements favoris' />) :
                    (<FlatList
                        data={elements}
                        extraData={favElements}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) => (
                            <ElementListItem
                                element={item}
                                onClick={navigateToFilmeDetails}
                                isFav={amIaFavFilme(item.id)} />
                        )}
                        refreshing={isRefreshing}
                        onRefresh={refreshFavFilms}
                    />)
            }
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        favElements: state.favElementsID
    }
}
export default connect(mapStateToProps)(FavElements);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 12,
        marginTop: 16,
    },
});
