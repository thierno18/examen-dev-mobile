import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, ActivityIndicator, Button, ScrollView, Image, FlatList} from 'react-native';

import {connect} from 'react-redux';

import DisplayError from "./DisplayError";
import {getFilmDetails} from "../api/myApi";
import Colors from "../definitions/Colors";
import Assets from "../definitions/Assets";


const Element = ({route, favElements, dispatch}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [element, setElement] = useState(null);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        requestElement();
    }, []);

    const requestElement = async () => {
        try {
            const apiElementResult = await getFilmDetails(route.params.elementID);
            setElement(apiElementResult);
            setIsLoading(false);
        } catch (error) {
            setIsError(true);
        }
    }

    const saveElement = async () => {
        const action = {type: 'SAVE_ELEMENT', value: route.params.elementID};
        dispatch(action);
    }

    const unsaveElement = async () => {
        const action = {type: 'UNSAVE_ELEMENT', value: route.params.elementID};
        dispatch(action);
    }

    const displaySaveElement = () => {
        if (favElements.findIndex(i => i === route.params.elementID) !== -1) {
            return (
                <Button
                    title='Retirer des favoris'
                    color={Colors.mainGreen}
                    onPress={unsaveElement}
                />
            );
        }
        return (
            <Button
                title='Ajouter aux favoris'
                color={Colors.mainGreen}
                onPress={saveElement}
            />
        );
    }


    return (
        <View style={styles.container}>
            {isError ?
                (<DisplayError message='Impossible de récupérer les données du film'/>) :
                (isLoading ?
                        (<View style={styles.containerLoading}>
                            <ActivityIndicator size="large"/>
                        </View>) :
                        (<ScrollView style={styles.containerScroll}>
                            <View style={styles.containerHedear}>
                                <Image source={{uri: "https://image.tmdb.org/t/p/w500/" + element.poster_path}}   style={styles.thumbnail}/>
                                <View style={styles.informationContainer}>
                                    <View style={styles.titleContainer}>
                                        <Text style={styles.title}>
                                            {element.original_title}
                                        </Text>
                                        <Text style={[styles.data, styles.text]}
                                              numberOfLines={1}>
                                            {element.release_date}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.statContainer}>
                                <Text style={[styles.data]}>
                                    {element.overview}
                                </Text>
                            </View>
                            {displaySaveElement()}
                        </ScrollView>)
                )}
        </View>
    )
};

const mapStateToProps = (state) => {
    return {
        favElements: state.favElementsID
    }
}

export default connect(mapStateToProps)(Element);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerHedear: {
        flexDirection: 'row',
        paddingVertical: 8,
    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerScroll: {
        flex: 1,
        paddingHorizontal: 12,
        paddingVertical: 16,
    },
    informationContainer: {
        flex: 1,
        marginLeft: 12,
        justifyContent: 'center',
    },
    text: {
        fontStyle: 'italic',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    thumbnail: {
        width: 128,
        height: 128,
        borderRadius: 12,
        backgroundColor: Colors.mainGreen,
    },
});
