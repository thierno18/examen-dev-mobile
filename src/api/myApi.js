const API_KEY = '8b7409aeca2ff954be2f7d59b72bd3f8';

export async function getElements(searchTerm = '',offset = 0) {
    if(searchTerm=='') searchTerm ='Movies';
    try {
        const myHeaders = new Headers({ 'user-key': API_KEY });
        const url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=${searchTerm}&page=1&include_adult=false`
        const response = await fetch(url, { headers: myHeaders });
        const json = await response.json();
        return json;
    } catch (error) {
        console.log(`Error with function getRestaurants ${error.message}`);
        throw error;
    }
};

export async function getFilmDetails(filmID) {
    try {
        const myHeaders = new Headers({ 'user-key': API_KEY });
        const url = `https://api.themoviedb.org/3/movie/${filmID}?api_key=8b7409aeca2ff954be2f7d59b72bd3f8&language=en-US`;
        const response = await fetch(url, { headers: myHeaders });
        const json = await response.json();
        return json;
    } catch (error) {
        console.log(`Error with function getRestaurantDetails ${error.message}`);
        throw error;
    }
};
