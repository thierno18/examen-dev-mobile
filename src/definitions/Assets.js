import iconError from '../../assets/error.png';
import iconFav from '../../assets/favFull.png'
import iconFavEmty from '../../assets/favEmpty.png'
import iconMissingIMG from '../../assets/missingImage.png';



const Assets = {
    icons: {
        error: iconError,
        fav : iconFav,
        emty: iconFavEmty,
        missingIMG: iconMissingIMG,
    },
};

export default Assets;
