import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image } from 'react-native';

import Home from "../components/Home";
import Element from "../components/Element";
import FavElements from "../components/FavElements";

import Colors from "../definitions/Colors";
import Assets from "../definitions/Assets";

const HomeNavigation = createStackNavigator();
const FavNavigation = createStackNavigator();
const TabNavigation = createBottomTabNavigator();

function homeStackScreens() {
    return (
        <HomeNavigation.Navigator
            initialRouteName="ViewHome"
        >
            <HomeNavigation.Screen
                name="ViewHome"
                component={Home}
                options={{ title: 'Search' }}
            />
            <HomeNavigation.Screen
                name="ViewFilme"
                component={Element}
                options={{ title: 'Movies' }}
            />
        </HomeNavigation.Navigator>
    )
};

function favStackScreens() {
    return (
        <FavNavigation.Navigator
            initialRouteName="ViewFav"
        >
            <FavNavigation.Screen
                name="ViewFav"
                component={FavElements}
                options={{ title: 'Watched' }}
            />
            <FavNavigation.Screen
                name="ViewFilme"
                component={Element}
                options={{ title: 'Element' }}
            />
        </FavNavigation.Navigator>
    )
};


function RootStack() {
    return (
        <TabNavigation.Navigator
            tabBarOptions={{
                activeTintColor: Colors.mainGreen,
            }}>
            <TabNavigation.Screen
                name="Movies"
                component={homeStackScreens}
                options={() => ({
                    tabBarIcon: ({ color }) => {
                        return <Image source={Assets.icons.search} style={{ tintColor: color }} />;
                    }
                })}
            />
            <TabNavigation.Screen
                name="Watched"
                component={favStackScreens}
                options={() => ({
                    tabBarIcon: ({ color }) => {
                        return <Image source={Assets.icons.fav} style={{ tintColor: color }} />;
                    }
                })}
            />

        </TabNavigation.Navigator>
    );
}

export default RootStack
